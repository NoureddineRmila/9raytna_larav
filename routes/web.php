<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
    
});

//fullcalender
Route::get('admin/fullcalendar','FullCalendarController@index');
Route::post('admin/fullcalendar/create','FullCalendarController@create');
Route::post('admin/fullcalendar/update','FullCalendarController@update');
Route::post('admin/fullcalendar/delete','FullCalendarController@destroy');