<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Lesson;
use Faker\Generator as Faker;

$factory->define(Lesson::class, function (Faker $faker) {
    return [
        'course_id' => $faker->word,
        'course' => $faker->word,
        'titre' => $faker->word,
        'content' => $faker->paragraphs(3, true),
        'file' => $faker->word,
        'start_time' => $faker->dateTime(),
        'end_time' => $faker->dateTime(),
    ];
});
