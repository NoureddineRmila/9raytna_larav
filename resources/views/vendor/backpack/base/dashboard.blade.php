@extends(backpack_view('blank'))
 

@section('content')
<div class="container-fluid">
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-sm-6 col-lg-3">
          <div class="card text-white bg-primary">
            <div class="card-body pb-0">
              <div class="btn-group float-right">
                <button class="btn btn-transparent dropdown-toggle p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="icon-settings"></i></button>
                <div class="dropdown-menu dropdown-menu-right" style=""><a class="dropdown-item" href="#">Action</a><a class="dropdown-item" href="#">Another action</a><a class="dropdown-item" href="#">Something else here</a></div>
              </div>
              <div class="text-value">9.823</div>
              <div>Members online</div>
            </div>
            <div class="chart-wrapper mt-3 mx-3" style="height:70px;"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
              <canvas class="chart chartjs-render-monitor" id="card-chart1" height="87" style="display: block; height: 70px; width: 254px;" width="317"></canvas>
            <div id="card-chart1-tooltip" class="chartjs-tooltip bottom" style="opacity: 0; left: 199.537px; top: 147.396px;"><div class="tooltip-header"><div class="tooltip-header-item">July</div></div><div class="tooltip-body"><div class="tooltip-body-item"><span class="tooltip-body-item-color" style="background-color: rgba(255, 255, 255, 0.55);"></span><span class="tooltip-body-item-label">My First dataset</span><span class="tooltip-body-item-value">40</span></div></div></div></div>
          </div>
        </div>
        <!-- /.col-->
        <div class="col-sm-6 col-lg-3">
          <div class="card text-white bg-success">
            <div class="card-body pb-0">
              <button class="btn btn-transparent p-0 float-right" type="button"><i class="icon-location-pin"></i></button>
              <div class="text-value">9.823</div>
              <div>Members online</div>
            </div>
            <div class="chart-wrapper mt-3 mx-3" style="height:70px;"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
              <canvas class="chart chartjs-render-monitor" id="card-chart2" height="87" width="315" style="display: block; height: 70px; width: 252px;"></canvas>
            <div id="card-chart2-tooltip" class="chartjs-tooltip bottom top" style="opacity: 0; left: 128.353px; top: 123.661px;"><div class="tooltip-header"><div class="tooltip-header-item">February</div></div><div class="tooltip-body"><div class="tooltip-body-item"><span class="tooltip-body-item-color" style="background-color: rgba(255, 255, 255, 0.55);"></span><span class="tooltip-body-item-label">My First dataset</span><span class="tooltip-body-item-value">18</span></div></div></div></div>
          </div>
        </div>
        <!-- /.col-->
        <div class="col-sm-6 col-lg-3">
          <div class="card text-white bg-warning">
            <div class="card-body pb-0">
              <div class="btn-group float-right">
                <button class="btn btn-transparent dropdown-toggle p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="icon-settings"></i></button>
                <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="#">Action</a><a class="dropdown-item" href="#">Another action</a><a class="dropdown-item" href="#">Something else here</a></div>
              </div>
              <div class="text-value">9.823</div>
              <div>Members online</div>
            </div>
            <div class="chart-wrapper mt-3" style="height:70px;"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
              <canvas class="chart chartjs-render-monitor" id="card-chart3" height="87" width="357" style="display: block; height: 70px; width: 286px;"></canvas>
            </div>
          </div>
        </div>
        <!-- /.col-->
        <div class="col-sm-6 col-lg-3">
          <div class="card text-white bg-dark">
            <div class="card-body pb-0">
              <div class="btn-group float-right">
                <button class="btn btn-transparent dropdown-toggle p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="icon-settings"></i></button>
                <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="#">Action</a><a class="dropdown-item" href="#">Another action</a><a class="dropdown-item" href="#">Something else here</a></div>
              </div>
              <div class="text-value">9.823</div>
              <div>Members online</div>
            </div>
            <div class="chart-wrapper mt-3 mx-3" style="height:70px;"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
              <canvas class="chart chartjs-render-monitor" id="card-chart4" height="87" width="315" style="display: block; height: 70px; width: 252px;"></canvas>
            </div>
          </div>
        </div>
        <!-- /.col-->
      </div>
      <!-- /.row--> 
      
    </div>
  </div>
 

@endsection  
@include('fullcalendar');
